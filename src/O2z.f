!==========================================================================
! The O2z - model, implemented in FORTRAN
! Tom Cox, UAntwerpen.
! based on
!THE ESTUARINE BOD_O2 model, implemented in FORTRAN
! Karline Soetaert, nioz-yerseke  VERSION WITH FORCINGS
!==========================================================================

!==========================================================================
! initialise the common block with parameter values
! 
!==========================================================================

      SUBROUTINE initO2z(steadyparms)
      IMPLICIT NONE
      EXTERNAL steadyparms

      INTEGER,PARAMETER :: N = 40
      INTEGER,PARAMETER :: npar = 9      
      INTEGER,PARAMETER :: nc = npar

      DOUBLE PRECISION parms(nc)
      COMMON /myparms/parms

       CALL steadyparms(nc, parms)
       
      RETURN
      END SUBROUTINE

!==========================================================================
! Initialise the forcing function common block (boundary conditions)
!==========================================================================

      SUBROUTINE initforc (steadyforcs)
      IMPLICIT NONE
      EXTERNAL steadyforcs

      INTEGER,PARAMETER :: nf = 1 

      DOUBLE PRECISION forcs(nf)
      COMMON /myforcs/forcs

       CALL steadyforcs(nf, forcs)
       
      RETURN
      END SUBROUTINE

!==========================================================================
!==========================================================================
! subroutine calculating the rate of change of
! the O2z model - here the quantities in the common blocks are named
!==========================================================================
!==========================================================================

      SUBROUTINE O2z (neq, t, Conc, dConc, yout, ip)
      IMPLICIT NONE

!......................... declaration section.............................
      INTEGER           :: neq, ip(*), i
      INTEGER,PARAMETER :: N=40

      DOUBLE PRECISION :: t, Conc(2*N), dConc(2*N), yout(*)

      DOUBLE PRECISION :: B(N), O2(N)
      DOUBLE PRECISION :: dB(N),dO2(N)

      DOUBLE PRECISION :: CChl, KO2, Rm, Pm, a, k, OxSat
      DOUBLE PRECISION :: E0, E(N)
      DOUBLE PRECISION :: fE0, ftemp
      DOUBLE PRECISION :: O2sat, temp
      DOUBLE PRECISION :: D, dx
      DOUBLE PRECISION :: Bin, Bout, O2in, O2out


      COMMON /myparms /CChl, KO2, Rm, Pm, a, k, OxSat,                           &
     &                 D, dx
      
      COMMON /myforcs /E0

! output variables
      DOUBLE PRECISION :: GP(N), NP(N)
    
      COMMON /myout  /GP, NP, fE0
                                                                             
      CHARACTER(len=80) msg

!............................ statements ..................................

!     check memory allocated to output variables
      IF (ip(1) < 2*N)  CALL rexit("nout should be at least 40") 

! from Conc to O2, B
      DO I = 1, N
	Conc(I) = max(0.D0,Conc(I))
	Conc(N+I) = max(0.D0,Conc(N+I))
        O2(I) = Conc(I)
        B(I)  = Conc(N+I)
      ENDDO
           
! --------------------------------------------------------------------------
! Forcing functions
! --------------------------------------------------------------------------

      fE0       = E0   
      ftemp     = temp

	CALL tran1d (N, B, 0.D0, 0.D0, 0.D0, 0.D0, 1, 1, D, dx, dB,             & 
     &			Bin, Bout)
	CALL tran1d (N, O2, 0.D0, 0.D0, KO2 * (OxSat - O2(1)) , 0.D0,           &
     &			1, 1, D, dx, dO2, O2in, O2out)

      DO I = 1, N
	E(I) = fE0 * exp(-k*(I-0.5)*dx)
        GP(I) = B(I) * Pm * (1-exp(-a/Pm*E(I)))
	NP(I) = GP(I) - Rm*B(I)
      ENDDO

! --------------------------------------------------------------------------
! The total rate of change
! --------------------------------------------------------------------------
      dB = dB + NP/CChl  
      dO2  = dO2 + NP 

! --------------------------------------------------------------------------
! from dBOD, dO2,... to dconc
! --------------------------------------------------------------------------
      DO I = 1, N
         dConc(I)      =  dO2(I)
         dConc(N+I)    =  dB(I)  
      ENDDO
      
      CALL getout(yout)


	RETURN
	END SUBROUTINE


!==========================================================================
! put output variables in one vector
!==========================================================================

      SUBROUTINE getout(yout)
      DOUBLE PRECISION :: yout(*), out(81)
      INTEGER :: i

      COMMON /myout    /out
      DO i = 1, 81
       yout(i) = out (i)
      ENDDO       
 
      END SUBROUTINE getout
       

!#==============================================================================
! transport in a 1-dimensional finite volume grid WITH lateral input
! all inputs are vectors, upstream weighted advection; 
! Flow is one value, defined on upstream interface (Flowup)
! compile        system("R CMD SHLIB tran.1D.vol.f")
! load           dyn.load("tran.1D.vol.dll")
!#==============================================================================

      SUBROUTINE tran1dvolc (N, C, Cup, Cdown, Fup, Fdown, Flat, Clat,          &
     &                   BcUp, BcDown, D, FlowUp, Flows, Volume,                &
     &                   Flux, dC, Fin, Fout) 
      IMPLICIT NONE
      INTEGER N                  ! length of C
C input concentration
      DOUBLE PRECISION C(N)

C Boundary concentrations (used if Bc = 2), fluxes (used if Bc = 1) 
      DOUBLE PRECISION Cup, Cdown, Fup, Fdown 

C Diffusion, advection weighing, Volume
      DOUBLE PRECISION D(N+1),  Volume(N)

C Lateral input rate, L3/T and concentration
      DOUBLE PRECISION Flat(N), Clat(N)

C Flows
      DOUBLE PRECISION FlowUp, Flows(N+1)
      
C boundary concitions (1= flux, 2=conc, 3 = 0-gradient)
      INTEGER BcUp, BcDown   

C output: flux vector, rate of change, and flux in and out (mass/sec)
      DOUBLE PRECISION Flux(N+1), dC(N), Fin, Fout

C locals 
      INTEGER I

C -------------------------------------------------------------------------------

C Start with lateral input
      Flows(1) = FlowUp
      DO I = 1,N
        IF (Flat(I) > 0) THEN
          dC(I) = Clat(I)*Flat(I) / Volume(I)
        ELSE IF (Flat(I) < 0) THEN
          dC(I) = C(I)*Flat(I) / Volume(I)
        ELSE
          dC(I) = 0.D0
        ENDIF
        Flows(I+1) = Flows(I) + Flat(I)
      ENDDO

C Use vector Flat for the 
C Flux - first internal cells

      DO I = 2,N
        Flux(I) = -D(I) * (C(I)-C(I-1))  

        IF (Flows(I)  > 0) THEN
          Flux (I) = Flux(I) + Flows(I) *C(I-1)
        ELSE IF (Flows(I)  < 0) THEN
          Flux (I) = Flux(I) + Flows(I) *C(I)
        ENDIF 

      ENDDO

C Then the outer cells 
C upstream boundary
      IF (BcUp .EQ. 1) THEN
        Flux(1) = Fup

      ELSE IF (BcUp .EQ. 2) THEN
        Flux(1) = -D(1) * (C(1)-Cup) 

	      IF (Flows(1)  > 0) THEN
          Flux (1) = Flux(1) + Flows(1) * Cup
	      ELSE IF (Flows(1)  < 0) THEN
          Flux (1) = Flux(1) + Flows(1) * C(1)
        ENDIF 

      ELSE IF (BcUp .EQ. 3) THEN
        Flux(1) = 0.D0

      ELSE 
        Flux(1) = 0.D0

      ENDIF

C downstream boundary
      IF (BcDown .EQ. 1) THEN
        Flux(N+1) = Fdown

      ELSE IF (BcDown .EQ. 2) THEN
        Flux(N+1) = -D(N+1) * (Cdown-C(N)) 

	      IF (Flows(N+1)  > 0) THEN
          Flux (N+1) = Flux(N+1) + Flows(N+1)*C(N)
    	  ELSE IF (Flows(N+1) < 0) THEN
          Flux (N+1) = Flux(N+1) + Flows(N+1)*Cdown
        ENDIF 

      ELSE IF (BcDown .EQ. 3) THEN
        Flux(N+1) =0.D0

      ELSE
        Flux(N+1) =0.D0

      ENDIF


C Rate of change = negative flux gradient
      DO I = 1,N
        dC(I) = dC(I) -(Flux(I+1) - Flux(I)) / Volume(I)
      ENDDO

      Fin  = Flux(1) 
      Fout = Flux(N+1) 
	    
      RETURN
      END SUBROUTINE tran1dvolc


!#==============================================================================
! diffusive transport (no advection!) in a 1-dimensional finite volume grid 
! all inputs are vectors, upstream weighted advection; 
! 
! compile        system("R CMD SHLIB tran.1D.vol.f")
! load           dyn.load("tran.1D.vol.dll")
!#==============================================================================


      SUBROUTINE tran1d (N, C, Cup, Cdown, Fup, Fdown, BcUp,BcDown,             &
     & 			D, deltax, dC, Fin, Fout)  


      IMPLICIT NONE
      INTEGER N                  ! length of C
C input concentration
      DOUBLE PRECISION C(N)

C Boundary concentrations (used if Bc = 2), fluxes (used if Bc = 1) 
      DOUBLE PRECISION Cup, Cdown, Fup, Fdown 

C Diffusion, advection weighing, Volume
      DOUBLE PRECISION D, deltax
      
C boundary concitions (1= flux, 2=conc, 3 = 0-gradient)
      INTEGER BcUp, BcDown   

C output: flux vector, rate of change, and flux in and out (mass/sec)
      DOUBLE PRECISION dC(N), Fin, Fout

C locals 
      INTEGER I
	 DOUBLE PRECISION Flux(N+1)

C -------------------------------------------------------------------------------

C Use vector Flat for the 
C Flux - first internal cells

      DO I = 2,N
        Flux(I) = -D * (C(I)-C(I-1))/deltax

      ENDDO


C upstream boundary
      IF (BcUp .EQ. 1) THEN
        Flux(1) = Fup

      ELSE IF (BcUp .EQ. 2) THEN
        Flux(1) = -D * (C(1)-Cup)/deltax

      ELSE IF (BcUp .EQ. 3) THEN
        Flux(1) = 0.D0

      ELSE 
        Flux(1) = 0.D0

      ENDIF

C downstream boundary
      IF (BcDown .EQ. 1) THEN
        Flux(N+1) = Fdown

      ELSE IF (BcDown .EQ. 2) THEN
        Flux(N+1) = -D * (Cdown-C(N))/deltax

      ELSE IF (BcDown .EQ. 3) THEN
        Flux(N+1) =0.D0

      ELSE
        Flux(N+1) =0.D0

      ENDIF


C Rate of change = negative flux gradient
      DO I = 1,N
        dC(I) = -(Flux(I+1) - Flux(I)) / deltax
      ENDDO

      Fin  = Flux(1) 
      Fout = Flux(N+1) 

      RETURN
      END SUBROUTINE tran1D

! Test: the following is in particular needed for O2x model-runs, but included here to test DLL compilations
! Rewritten as a subroutine -> fortran functions are not callable from R
! Original function is below

      MODULE ModuleGaussInternPhyto

! *---Gauss weights for five point Gaussian interpolation
      INTEGER, PARAMETER :: IGAUSS = 5
      DOUBLE PRECISION   :: RelGAUSS(5),WGAUSS(5),XGauss(5)
    

! Gaussion depth Positions
      DATA XGAUSS /0.0469, 0.2308, 0.5000, 0.7692, 0.9531/
! Weights of each gaussian point
      DATA WGAUSS /0.1185, 0.2393, 0.2844, 0.2393, 0.1185/

      END MODULE ModuleGaussInternPhyto


      SUBROUTINE LightLimGaussian(k, Pmax, alpha, E0, morphology, d, LL)
      USE ModuleGaussInternPhyto
!USE ModuleMorphology
      IMPLICIT NONE
	INTEGER :: i, maxdepth
	double precision :: sumgrowth
	double precision :: depth,integrationdepth
	double precision, INTENT (IN) :: k,Pmax,alpha,E0, d
	double precision, INTENT (OUT) :: LL
	double precision :: dw,GPPz !!dw=depth*gaussian weight GPPz=Platt's primary production at depth z
	double precision :: morphology(0:14) !!morphology(i) = surface at depth of i in the same units as k
	double precision :: SGAUSS(IGAUSS)

 	
	maxdepth = 0
	do i=0,14
	if (morphology(i) == 0) exit
	maxdepth = i
	enddo 
	
	integrationdepth = MIN(2*log(10.)/k,dble(maxdepth)) 
									 !! depth for which light is 1% of incident light
									 !!	(exponential law), or depth of water

	! Call SurfaceGauss(morphology, integrationdepth, XGAUSS, SGAUSS) ! SurfaceGauss as subroutine in stead of function
										
	sumgrowth = 0

	Do i = 1,IGAUSS
		depth = integrationdepth*XGAUSS(i)
		dw = integrationdepth*WGAUSS(i)
		GPPz = (1-exp(-alpha*light(depth,k,E0)/Pmax))
		sumgrowth = sumgrowth + dw*GPPz*Surface(depth,morphology)
	EndDo
	LL = sumgrowth


        RETURN
	CONTAINS

	double precision FUNCTION light(depth,k,E0)

		Double Precision :: depth,k,E0
		light = E0*exp(-k*depth)
	END FUNCTION light 

	! SUBROUTINE SurfaceGauss(morphology, depth, XGAUSS, SGAUSS)
	! double precision :: depth, XGAUSS(5), SGAUSS(
	! END SUBROUTINE SurfaceGauss
	!************************************************************************
	!!!hiervan beter procedure maken, en een array surface(IGAUSS), want altijd hele loop doorlopen
	double precision FUNCTION Surface(depth,morphology)
		Double Precision :: depth
		Double precision :: morphology(0:14)
		INTEGER :: i
	
      do i = 1,maxdepth
        if (dble(i-1) <= depth .and. dble(i) > depth) then 
        Surface = morphology(i-1) +                                             &
     &		  (morphology(i)-morphology(i-1))*(depth-i+1)
        exit
        endif
       enddo		
      END FUNCTION Surface
	!*************************************************************************

      END SUBROUTINE LightLimGaussian 



      double precision FUNCTION LightLimGaussian_Old(k,T,Pmax,                      &
     & 		fClPmax,Cl,alpha,E0,morphology)
      USE ModuleGaussInternPhyto
!USE ModuleMorphology
      IMPLICIT NONE
	INTEGER :: i
	double precision :: sumgrowth
	double precision :: depth,integrationdepth,maxdepth
	double precision, INTENT (IN) :: k,T,PMax,alpha,E0,fClPmax,Cl
	double precision :: dw,GPPz !!dw=depth*gaussian weight GPPz=Platt's primary production at depth z
	double precision :: morphology(0:14) !!morphology(i) = surface at depth of i in the same units as k

 	
	maxdepth = 0
	do i=0,14
	if (morphology(i) == 0) exit
	maxdepth = i
	enddo 
	
	integrationdepth = MIN(2*log(10.)/k,dble(maxdepth)) 
									 !! depth for which light is 1% of incident light
									 !!	(exponential law), or depth of water
										
	sumgrowth = 0

	Do i = 1,IGAUSS
		depth = integrationdepth*XGAUSS(i)
		dw = integrationdepth*WGAUSS(i)
		GPPz = (PMax+fClPmax*Cl)*(1-exp(-alpha*light(                         &
     &			depth,k,E0)/Pmax))*Q10(T,10.D0,2.D0)
		sumgrowth = sumgrowth + dw*GPPz*Surface(depth,morphology)
		!sumgrowth = sumgrowth +  integrationdepth*WGAUSS(i)*PMax*(1-exp(-alpha*light(depth,k,E0)/PMax))*FRALG*Surface(depth,morphology)
	EndDo
	LightLimGaussian_Old = sumgrowth

	CONTAINS
	!************************************************************************
	double precision function Q10(t,tbase,q10val)
		double precision :: t,tbase,q10val

		Q10 = exp((t-tbase)*dlog(q10val)/10)

	end function q10


	double precision FUNCTION light(depth,k,E0)

		Double Precision :: depth,k,E0
		light = E0*exp(-k*depth)
	END FUNCTION light 

	!************************************************************************
	!!!hiervan beter procedure maken, en een array surface(IGAUSS), want altijd hele loop doorlopen
	double precision FUNCTION Surface(depth,morphology)
		Double Precision :: depth
		Double precision :: morphology(0:14)
		INTEGER :: i
	
      do i = 1,maxdepth
        if (dble(i-1) <= depth .and. dble(i) > depth) then 
        Surface = morphology(i-1) +                                             &
     &		  (morphology(i)-morphology(i-1))*(depth-i+1)
        exit
        endif
       enddo		
      END FUNCTION Surface
	!*************************************************************************

      END FUNCTION LightLimGaussian_Old








