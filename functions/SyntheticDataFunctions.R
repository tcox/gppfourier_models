#
# 2013-09-17 Auxiliary functions for selected model runs in SyntheticData.R 
# 2016-09-08 Model functions are now in SyntheticDataModels.R
# 2016-19-08 Plot functions are now in SyntheticDataPlotFunctions.R
# based on: 00SimpleModels/SimpleModelsFunctions.R

############33
# Auxiliary functions
##########

detrend <- function(y){
x <- 1:(length(y))
trendmod <- lm(y~x)
trend <- predict(trendmod)

return(y-trend)
}


truncsinpars <- function(AE0,fDL){
AEmean <- AE0/2
theta1 <- pi*(1-fDL)
aAr <- -cos(pi - theta1)	#ratio a over A in  pmax(0,a+A*cos(2*pi/T * t))
a1 <- AEmean/((sin(theta1) + (pi-theta1) * cos(theta1))/(pi-theta1 + 0.5 * sin(2*theta1)))
ATrunc <- a1*pi/(pi - theta1 + 0.5*sin(2*theta1))
aTrunc <- aAr*ATrunc

return(list(aTrunc=aTrunc,ATrunc=ATrunc,a1=a1,theta1=theta1))
}




##############
# Odum method
##############

Odum <- function(ox,oxsat,k,DN,deltat = 1,depth=5,Fup=NULL){
if (is.null(Fup)) {
reaer <- k*(oxsat-ox)/depth	#reaeration rate
} else reaer <- Fup/depth
SetRise <- as.numeric(c(FALSE,DN[1:(length(DN)-1)] != DN[2:length(DN)]))
DNnr <- cumsum(SetRise)
dox <- c(0,diff(ox))
sums <- tapply(dox-reaer[1:length(DNnr)]*deltat,DNnr,sum)
nights <- unique(DNnr[!DN])
Nlength <- tapply(DN,DNnr,length)[paste(nights)]
days <- unique(DNnr[DN])
Resp24 <- sums[paste(nights)]*1/(deltat*Nlength)
GP <- sums[paste(days)] 
GP <- GP - Resp24[as.character(as.numeric(names(GP))+1)]*(1-Nlength[as.character(as.numeric(names(GP))+1)]*deltat)	
#GP <- GP - Resp24[as.character(as.numeric(names(GP))+1)]

return(list(GP=GP,Resp=Resp24))
}


################
# Tidal Sampling
################

####Perform 'Tidal sampling' to simulate water masses passing by the sensor with the tides
#From Kruibeke sonde data, we know  that excursion is about 10 km (check excursion calculation in KruibekeCalculations.R)

TidalSampling <- function(O2, times=O2[,1],w = 2*pi/.52, dx=NULL,exc=5000, at = floor(dim(O2)[2]/2), v=NULL, S=NULL, dtSal=NULL, dtv = NULL, profile=NULL, dO2dx = FALSE) {

if (!is.null(S)) {
if (is.null(dtSal)) stop("Provide time step of salinity series (dtSal)")
if (diff(range(times))>length(S)*dtSal) {
  warning("Salinity series is shorter than required time range. Salinity will recycled")
  Nrecycle <- diff(range(times))%/%(length(S)*dtSal)
  S <- rep(S,(Nrecycle+1))
  }
}

if (!is.null(v)){
if (is.null(dtv)) stop("Provide time step of velocity time series (dtv)")
}

if (!is.null(v)) {
  excursion <- cumsum(v)*dtv
} else if (!is.null(S)){
if (is.null(profile)){
  excursion <- (S - mean(S))/(max(S) - mean(S)) * exc
  excursion <- approx((1:length(S))*dtSal,excursion,times)$y
  excursion <- excursion - mean(excursion) 	# center excursion around mean value of 0
  } else {
  excursion <- approx(x=profile$S, y = profile$x, xout = S,rule=2)$y
  excursion <- (excursion - mean(excursion))*1000 # distance in Cl-profile is in km
  excursion <- approx((1:length(excursion))*dtSal, excursion,times)$y
  }
} else {
excursion <- exc*cos(w*times)
}

locationi <- at + floor(excursion/dx)
O2tidal <- O2[,2]
dO2dxtidal <- O2[,2]
for (i in 1:length(times)){
  if (locationi[i] >= dim(O2)[2] - 1){
  # O2tidal[i] <- O2[i,locationi[i]]
  O2tidal[i] <- O2[i,dim(O2)[2]]
  dO2dxtidal[i] <- (O2[i,dim(O2)[2]] - O2[i,dim(O2)[2]-1])/dx
  } else if (locationi[i] < 1){
  O2tidal[i] <- O2[i,1]
  dO2dxtidal[i] <- (O2[i,2] - O2[i,1])/dx
  } else {
  O2tidal[i] <- O2[i,locationi[i]] + (at + excursion[i]/dx - locationi[i])*(O2[i,locationi[i] + 1] - O2[i,locationi[i]])
  dO2dxtidal[i] <- (O2[i,locationi[i] + 1] - O2[i,locationi[i]])/dx
  }

  
}

if (dO2dx) return(list(O2tidal, dO2dxtidal))
else return(O2tidal)
}

#
# dSdx calculation from time series and profile, used for Advective Correction of Fourier estimate
# 
dSdxfun <- function(S,profile, Smin = 0.3) {

if(is.unsorted(profile$S)){
profile$S <- rev(profile$S)
profile$x <- rev(profile$x)
}

dSInt <- findInterval(S,profile$S,all.inside=TRUE)
dS <- profile$S[dSInt+1] - profile$S[dSInt]
dx <- profile$x[dSInt+1] - profile$x[dSInt]
return(dS/dx)
}


