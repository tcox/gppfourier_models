# Models to create synthetic O2 time series to test Fourier method

Scripts SyntheticData1Dz.R and SyntheticData1DzDLL.R are example scripts for using the models and processing the output. 

## References:
Cox, T.; Soetaert, K.; Maris, T.; Kromkamp, J. & Meire, P.and Meysman, F. (2015), 'Estimating primary production from oxygen time series: a novel approach in the frequency domain', Limnology And Oceanography:Methods 13, 529-552.

Cox, T.; Soetaert, K. & van Beusekom, J. (2017), 'Tune in on 11.57 $\mu$Hz and listen to primary production', Biogeosciences 14, 5271-5280.

Tom J.S. Cox. (2017, September 22). GPPFourier v2.1 (Version v2.1). Zenodo. http://doi.org/10.5281/zenodo.940237



